package com.axeane.exceptions;

public class App {

  public static void main(String[] args) throws Exception {
    try (NewAutoClosableClass newClass = new NewAutoClosableClass()) {
      throw new SomeCoolException();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}
