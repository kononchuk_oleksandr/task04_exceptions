package com.axeane.exceptions;

public class SuperException extends Exception {

  private final String MESSAGE = "ARGH! This is Super Exception!";

  public SuperException() {
    System.out.println(getMessage());
  }

  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
