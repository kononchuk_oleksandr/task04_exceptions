package com.axeane.exceptions;

public class NewAutoClosableClass implements AutoCloseable {

  public NewAutoClosableClass() {
  }

  @Override
  public void close() throws Exception {
    throw new SuperException();
  }
}
