package com.axeane.exceptions;

public class SomeCoolException extends RuntimeException {

  private final String MESSAGE = "ARGH! This is Some Cool Exception!";

  public SomeCoolException() {
  }

  @Override
  public String getMessage() {
    return MESSAGE;
  }
}
